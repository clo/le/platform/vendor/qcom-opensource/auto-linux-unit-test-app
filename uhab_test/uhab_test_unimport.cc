/*
* Copyright (c) 2019~2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*    * Redistributions of source code must retain the above copyright
*      notice, this list of conditions and the following disclaimer.
*    * Redistributions in binary form must reproduce the above
*      copyright notice, this list of conditions and the following
*      disclaimer in the documentation and/or other materials provided
*      with the distribution.
*    * Neither the name of The Linux Foundation nor the names of its
*      contributors may be used to endorse or promote products derived
*      from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <stdarg.h>
#include "gtest/gtest.h"

extern "C" {
#include "habmm.h"
}
using ::testing::InitGoogleTest;
using ::testing::Test;
using ::testing::TestCase;
using ::testing::TestInfo;
using ::testing::TestPartResult;
using ::testing::UnitTest;

void * invalid_address = (void *) 0xffffff8000000000;
int32_t handle;
int32_t open_r;
int32_t recv_r;
int32_t import_r;
int32_t unimport_r;
int32_t close_r;
char export_id1;
char export_id2;
char export_id3;
uint32_t wrong_export_id1 = 1000;
uint32_t wrong_export_id2 = 2000;
uint32_t size_export_id = 1;
char *size_page = NULL;

namespace {

TEST(UhabSocketUnimportTestn, UhabUnimportn){
	open_r = habmm_socket_open(&handle, 601, 0, 0);
	printf("Test_habmm_unimport begin: habmm_socket_open the return value open_r=%d handle=0x%x\n", open_r, handle);
	if(open_r < 0)
	{
		goto err1;
	}
	recv_r = habmm_socket_recv(handle, &export_id1, &size_export_id, 0, 0);
	printf("Test_habmm_unimport : habmm_socket_recv the return value recv_r=%d  export_id1=%d\n", recv_r, export_id1);
	EXPECT_EQ(0, recv_r);

	recv_r = habmm_socket_recv(handle, &export_id2, &size_export_id, 0, 0);
	printf("Test_habmm_unimport : habmm_socket_recv the return value recv_r=%d  export_id2=%d\n", recv_r, export_id2);
	EXPECT_EQ(0, recv_r);

	printf("Test_habmm_unimport :Test_habmm_import size_bytes is 4096 and flag is 0x00000001\n");
	import_r = habmm_import(handle, (void **)&size_page, 4096, export_id1, HABMM_IMPORT_FLAGS_CACHED);
	printf("Test_habmm_unimport: Test_habmm_import size_bytes is 4096 and flag is 0x00000001 the return value import_r=%d export_id1=%d \n", import_r, export_id1);
	EXPECT_EQ(0, import_r);

	import_r = habmm_import(handle, (void **)&size_page, 4096, export_id2, 0x00000000);
	printf("Test_habmm_unimport: Test_habmm_import size_bytes is 4096 and flag is 0x00000000 the return value import_r=%d export_id2=%d \n", import_r, export_id2);
	EXPECT_EQ(0, import_r);

	/* Negative testing */
	/* 1.input invalid export_id   */
	printf("1.Test_habmm_unimport Negative testing :input invalid export_id  flag=0x00000000\n");
	unimport_r = habmm_unimport(handle, wrong_export_id1, (void *)size_page, 0x00000000);
	printf("1.Test_habmm_unimport Negative testing the return value unimport_r=%d  flag=0x00000000\n", unimport_r);

	printf("1.Test_habmm_unimport Negative testing :input invalid export_id  flag=0x00000000\n");
	unimport_r = habmm_unimport(handle, wrong_export_id2, (void *)size_page, 0x00000000);
	printf("1.Test_habmm_unimport Negative testing the return value unimport_r=%d  flag=0x00000000\n", unimport_r);

	/* 2.input invalid handle   */
	printf("2.Test_habmm_unimport Negative testing :input invalid handle  flag=0x00000000\n");
	unimport_r = habmm_unimport(1, export_id1, (void *)size_page, 0x00000000);
	printf("2.Test_habmm_unimport Negative testing the return value unimport_r=%d  flag=0x00000000\n", unimport_r);

	printf("2.Test_habmm_unimport Negative testing :input invalid handle  flag=0x00000000\n");
	unimport_r = habmm_unimport(1, export_id2, (void *)size_page, 0x00000000);
	printf("2.Test_habmm_unimport Negative testing the return value unimport_r=%d  flag=0x00000000\n", unimport_r);

	/* 3.input error buff_shared  or  invalid address  */
	printf("3.Test_habmm_unimport Negative testing :input invalid address  flag=0x00000000\n");
	unimport_r = habmm_unimport(handle, export_id1, invalid_address, 0x00000000);
	printf("3.Test_habmm_unimport Negative testing the return value unimport_r=%d  flag=0x00000000\n", unimport_r);

	printf("3.Test_habmm_unimport Negative testing :input invalid address  flag=0x00000000\n");
	unimport_r = habmm_unimport(handle, export_id2, invalid_address, 0x00000000);
	printf("3.Test_habmm_unimport Negative testing the return value unimport_r=%d  flag=0x00000000\n", unimport_r);

	close_r = habmm_socket_close(handle);
	EXPECT_EQ(0, close_r);
	printf("Test_habmm_unimport close  the return value close_r=%d  \n", close_r);

	err1:
		printf("Can't open the mmid socket1\n");
}

TEST(UhabSocketUnimportTestp, UhabUnimportp){
	open_r = habmm_socket_open(&handle, 601, 0,0);
	printf("Test_habmm_unimport begin: habmm_socket_open the return value open_r=%d  \n", open_r);
	if(open_r < 0)
	{
		goto err;
	}
	recv_r = habmm_socket_recv(handle, &export_id1, &size_export_id, 10, 0);
	printf("Test_habmm_unimport : habmm_socket_recv the return value recv_r=%d  export_id1=%d\n", recv_r, export_id1);
	EXPECT_EQ(0, recv_r);

	recv_r = habmm_socket_recv(handle, &export_id2, &size_export_id, 10, 0);
	printf("Test_habmm_unimport : habmm_socket_recv the return value recv_r=%d  export_id2=%d\n", recv_r, export_id2);
	EXPECT_EQ(0, recv_r);

	printf("Test_habmm_unimport :Test_habmm_import size_bytes is 4096 and flag is 0x00000001\n");
	import_r = habmm_import(handle, (void **)&size_page, 4096, export_id1, HABMM_IMPORT_FLAGS_CACHED);
	printf("Test_habmm_unimport: Test_habmm_import size_bytes is 4096 and flag is 0x00000001 the return value import_r=%d export_id1=%d \n", import_r, export_id1);
	EXPECT_EQ(0, import_r);

	import_r = habmm_import(handle, (void **)&size_page, 4096, export_id2, 0x00000000);
	printf("Test_habmm_unimport: Test_habmm_import size_bytes is 4096 and flag is 0x00000000 the return value import_r=%d export_id2=%d \n", import_r, export_id2);
	EXPECT_EQ(0, import_r);

	import_r = habmm_import(handle, (void **)&size_page, 4096, export_id3, 0x00000000);
	printf("Test_habmm_unimport: Test_habmm_import size_bytes is 4096 and flag is 0x00000000 the return value import_r=%d export_id3=%d \n", import_r, export_id3);
	EXPECT_EQ(0, import_r);

	/* Positive testing */
	printf("Test_habmm_unimport Positive testing:\n");
	unimport_r = habmm_unimport(handle, export_id1, (void *)size_page, HABMM_EXP_MEM_TYPE_DMA);
	printf("Test_habmm_unimport Positive testing  the return value unimport_r=%d export_id1=%d flag=0x00000001\n", unimport_r, export_id1);
	EXPECT_EQ(0, unimport_r);

	printf("Test_habmm_unimport Positive testing:\n");
	unimport_r = habmm_unimport(handle, export_id2, (void *)size_page, 0x00000000);
	printf("Test_habmm_unimport Positive testing  the return value unimport_r=%d export_id2=%d flag=0x00000000\n", unimport_r, export_id2);
	EXPECT_EQ(0, unimport_r);

	printf("Test_habmm_unimport Positive testing:\n");
	unimport_r = habmm_unimport(handle, export_id3, (void *)size_page, HABMM_EXPIMP_FLAGS_FD );
	printf("Test_habmm_unimport Positive testing  the return value unimport_r=%d export_id3=%d flag=0x00010000\n", unimport_r, export_id3);
	EXPECT_EQ(0, unimport_r);

	close_r = habmm_socket_close(handle);
	EXPECT_EQ(0, close_r);
	printf("Test_habmm_unimport close  the return value close_r=%d  \n", close_r);

	err:
		printf("Can't open the mmid socket\n");
}

}  // namespace

int main(int argc, char **argv) {
	InitGoogleTest();
	return RUN_ALL_TESTS();
}
